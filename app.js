//Carregando módulos
const express = require('express')
const handlebars = require('express-handlebars')
const bodyParser = require('body-parser')
const app = express()
const path = require('path')
const mongoose = require('mongoose')
const session = require('express-session')
const flash = require('connect-flash')
//Rotas
const admin = require('./routes/admin')
const usuarios = require('./routes/usuario')
//Modelos
require('./models/Postagem')
require('./models/Categoria')
const Postagem = mongoose.model('postagens')
const Categoria = mongoose.model('categorias')
//Middleware de autenticação
const passport = require('passport')
require('./config/auth')(passport)
//Database configuration
const db = require('./config/db')

//Configurações
//Sessão
app.use(session({
   secret: "cursodenode",
   resave: true,
   saveUninitialized: true
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())

//Middleware
app.use((req, res, next) => {
   //Variaveis globais
   res.locals.success_msg = req.flash("success_msg")
   res.locals.error_msg = req.flash("error_msg")
   res.locals.error = req.flash("error")
   //req.user é automaticamente criada pelo passport
   //quando o usuário logar no sistema
   res.locals.user = req.user || null
   next()
})

//Body Parse
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

//Handlebars
app.engine('handlebars', handlebars({defaultLayout: 'main'}))
app.set('view engine', 'handlebars')

//Mongoose
mongoose.Promise = global.Promise;
mongoose.connect(db.mongoURI).then(() => {
   console.log('Conectado ao mongo')
}).catch((err) => {
   console.log(`Erro ao se conectar: ${err}`)
})

//Public
app.use(express.static(path.join(__dirname, 'public')))

//Rotas
app.get('/', (req, res) => {
   Postagem.find().populate('categoria').sort({createdAt: "desc"}).then(postagens => {
      res.render('index', {postagens})
   }).catch(error => {
      req.flash('error_msg', 'Houve um erro interno')
      res.redirect('/404')
   })
})

app.get("/postagem/:slug", (req, res) => {
   Postagem.findOne({slug: req.params.slug}).then(postagem => {
      if(postagem){
         res.render("postagem/index", {postagem})
      } else {
         req.flash("error_msg", "Esta postagem não existe")
         res.redirect('/')
      }
   }).catch(error => {
      req.flash("error_msg", "Houve um erro interno")
      res.redirect('/')
   })
})

app.get('/categorias', (req, res) => {
   Categoria.find().then(categorias => {
      res.render('categorias/index', {categorias})
   }).catch(error => {
      req.flash("error_msg", "Houve um erro interno ao listar as categorias")
      res.redirect('/')
   })
})

app.get('/categorias/:slug', (req, res) => {
   Categoria.findOne({slug: req.params.slug}).then(categoria => {
      if(categoria){
         Postagem.find({categoria: categoria._id}).then(postagens => {
            res.render('categorias/postagens', {postagens, categoria})
         }).catch(error => {
            req.flash("error_msg", "Houve um erro ao listar os posts!")
            res.redirect('/')
         })
      }else{
         req.flash("error_msg", "Esta categoria não existe")
         res.redirect('/')
      }
   }).catch(error => {
      req.flash("error_msg", "Houve um erro interno ao carregar a página desta categorias")
      res.redirect('/')
   })
})

app.get('/404', (req, res) => {
   res.send('Erro 404')
})


app.use('/admin', admin)
app.use('/usuarios', usuarios)

//Outros
//process.env.PORT return de random port created by Heroku
const PORT = process.env.PORT || 3001
app.listen(PORT, () => {
   console.log("Servidor rodando!")
})