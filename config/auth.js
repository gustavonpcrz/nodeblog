const localStategy = require("passport-local").Strategy
const mongoose = require("mongoose")
const bcrypt = require("bcryptjs")

//Model de usuaário
require('../models/Usuario')
const Usuario = mongoose.model('usuarios')

module.exports = passport => {
   passport.use(new localStategy({usernameField: 'email', passwordField: 'senha'}, (email, senha, done) => {
      Usuario.findOne({email: email}).then(usuario => {
         if(!usuario){
            return done(null, false, {message: "Esta conta não existe"})
         }

         bcrypt.compare(senha, usuario.senha, (erro, equals) => {
            if(equals){
               return done(null, usuario)
            }else{
               return done(null, false, {message: "Senha incorreta"})
            }
         })
      })
   }))
   //Salva os dados do usuário na seção
   passport.serializeUser((usuario, done) => {
      done(null, usuario.id)
   })
   passport.deserializeUser((id, done) => {
      Usuario.findById(id, (err, usuario) => {
         done(err, usuario)
      })
   }) 

}