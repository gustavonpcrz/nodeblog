const express = require('express')
const router = express.Router()
const mongoose = require('mongoose')
//Models
require('../models/Categoria')
require('../models/Postagem')
const Categoria = mongoose.model("categorias")
const Postagem = mongoose.model("postagens")
//Helpers
const { isAdmin } = require("../helpers/isAdmin")

router.get('/', isAdmin, (req, res) => {
   res.render("admin/index")
})

router.get('/posts', isAdmin, (req, res) => {
   res.send("Pagina de posts")
})

router.get("/categorias", isAdmin, (req, res) => {
   Categoria.find().sort({slug: 'desc'}).then(categorias => {
      res.render("admin/categorias", {categorias: categorias})
   }).catch(error => {
      req.flash("error_msg", "Houve um erro ao listar as categorias")
      res.redirect("/admin")
   })
})

router.get("/categorias/add", isAdmin, (req, res) => {
   res.render("admin/addcategorias")
})

router.post("/categorias/new", isAdmin, (req, res) =>{
   let errors = []

   if(!req.body.nome){
      errors.push({text: "Nome inválido"})
   }
   if(!req.body.slug){
      errors.push({text: "Slug inválido"})
   }
   if(req.body.nome.length < 2) {
      errors.push({text: "Nome da categoria é muito pequena"})
   }

   if( errors.length > 0 ){
      res.render("admin/addcategorias", {errors: errors})
   } else {
      const novaCategoria = {
         nome: req.body.nome,
         slug: req.body.slug
      }
   
      new Categoria(novaCategoria).save().then(() => {
         req.flash("success_msg", "Categoria criada com sucesso")
         res.redirect("/admin/categorias")
      }).catch(error => {
         req.flash("error_msg", "Houve um erro ao salvar a categoria, tente novamente")
         res.redirect("/admin")
      })

   }
})

router.get("/categorias/edit/:id", isAdmin, (req, res) => {
   Categoria.findOne({_id: req.params.id}).then(categoria => {
      res.render("admin/editcategorias", {categoria})
   }).catch(error => {
      req.flash("error_msg", "Esta categoria não existe")
      res.redirect("/admin/categorias");
   })
})

router.post("/categorias/edit", isAdmin, (req, res) => {
   let errors = []

   if(!req.body.nome){
      errors.push({text: "Nome inválido"})
   }
   if(!req.body.slug){
      errors.push({text: "Slug inválido"})
   }
   if(req.body.nome.length < 2) {
      errors.push({text: "Nome da categoria é muito pequena"})
   }

   if( errors.length > 0 ){
      res.render("admin/addcategorias", isAdmin, {errors: errors})
   } else {
      Categoria.findOne({_id: req.body.id}).then(categoria => {
         categoria.nome = req.body.nome
         categoria.slug = req.body.slug
         categoria.save().then(() => {
            req.flash("success_msg", "Categoria editada com sucesso!")
            res.redirect("/admin/categorias")
         }).catch(error => {
            req.flash("error_msg", "Houve um erro interno ao salvar a edição da categoria")
            res.redirect("/admin/categorias")
         })
      }).catch(error => {
         req.flash("error_msg", "Houve um erro ao editar a categoria")
         res.redirect("/admin/categorias")
      })
   }
})

router.post("/categorias/deletar", isAdmin, (req, res) => {
   Categoria.remove({_id: req.body.id}).then(() => {
      req.flash("success_msg", "Categoria excluída com sucesso!")
      res.redirect("/admin/categorias")
   }).catch(error => {
      req.flash("error_msg", "Houve um erro ao excluir a categoria")
      res.redirect("/admin/categorias")
   })
})

router.get("/postagens", isAdmin, (req, res) => {
   Postagem.find().populate("categoria").sort({createdAt: "desc"}).then(postagens => {
      res.render("admin/postagens", {postagens})
   }).catch(error => {
      req.flash("error_msg", "Houve um erro ao listar as postagens")
      res.redirect("/admin/")
   })
})

router.get("/postagens/add", isAdmin, (req, res) => {
   Categoria.find().then(categorias => {
      res.render("admin/addpostagem", {categorias})
   }).catch(error => {
      req.flash("error_msg", "Houve um erro ao carregar o formulário")
      res.redirect("/admin/postagens")
   })
})

router.post("/postagens/new", isAdmin, (req, res) => {
   var errors = []

   if(req.body.categoria == "0"){
      errors.push({text: "Cateforia inválida, registre uma categoria"})
   }

   if(errors.length > 0){
      res.render("admin/addpostagem", isAdmin, {errors})
   }else {
      const newPost = {
         titulo: req.body.titulo,
         descricao: req.body.descricao,
         conteudo: req.body.conteudo,
         categoria: req.body.categoria,
         slug: req.body.slug
      }

      new Postagem(newPost).save().then(() => {
         req.flash("success_msg", "Postagem criada com sucesso!")
         res.redirect("/admin/postagens")
      }).catch(error => {
         req.flash("erros_msg", "Houve um erro durante o salvamento da postagem")
         res.redirect("/admin/postagens")
      })
   }
})

router.get("/postagens/edit/:id", isAdmin, (req, res) => {
   Postagem.findOne({_id: req.params.id}).then(postagem => {
      Categoria.find().then(categorias => {
            res.render("admin/editpostagens", {categorias, postagem})
         }).catch(error => {
            req.flash("error_msg", "Houve um erro ao listar as categorias")
            res.redirect("/admin/postagens")
      })
   }).catch(error => {
      req.flash("error_msg", "Houve um erro ao carregar o formulário de edição")
      res.redirect("/admin/postagens")
   })
})

router.post("/postagens/edit", isAdmin, (req, res) => {
   Postagem.findOne({_id: req.body.id}).then(postagem => {
      postagem.titulo = req.body.titulo
      postagem.slug = req.body.slug
      postagem.descricao = req.body.descricao
      postagem.conteudo = req.body.conteudo
      postagem.categoria = req.body.categoria

      postagem.save().then(() => {
         req.flash("success_msg", "Postagem editada com sucesso!")
         res.redirect("/admin/postagens")
      }).catch(error => {
         req.flash("error_msg", "Houve interno")
         res.redirect("/admin/postagens")
      })

   }).catch(error => {
      req.flash("error_msg", "Houve um erro ao salvar a edição")
      res.redirect("/admin/postagens")
   })
})

router.get("/postagens/deletar/:id", isAdmin, (req, res) => {
   Postagem.remove({_id: req.params.id}).then(() => {
      req.flash("success_msg", "Postagem excluida com sucesso")
      res.redirect("/admin/postagens")
   }).catch((error) => {
      req.flash("error_msg", "Houve um erro interno")
      res.redirect("/admin/postagens")
   })
})

module.exports = router