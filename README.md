# NodeBlog

This project i build as a learning project from the Node.js course from Guia do Programador, for more information about the course provider access https://www.youtube.com/channel/UC_issB-37g9lwfAA37fy2Tg/featured

## Installation

To install project you need first have mongo install and configured in you machine, you can download mongo in the following url: https://www.mongodb.com/download-center?jmp=nav

After install mongo, open terminal andcreate the database blogapp:
```mongo
mongod //to initiate the mongo service, then open another terminal
mongo  //to open the mongo manager
use blogapp //create the database
```


with download this repo and execute the following commands.

```bash
npm install
```

if you prefer yarn
```bash
yarn
```

Simple as that!

