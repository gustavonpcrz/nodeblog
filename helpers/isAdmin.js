module.exports = {
   isAdmin: (req,res, next) => {
      //req.isAuthenticated() => function created by passport to know if the user is authenticated
      if(req.isAuthenticated() && req.user.eAdmin == 1){
         return next();
      }
      req.flash("error_msg", "Você precisar ser administrador!")
      res.redirect("/")
   }
}